package br.com.eduardo.dscatalog.entities;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "tb_category")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Category implements Serializable {
    private static final long serialVersionUID = -2691712369324330732L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
}
